describe Parenthino::Scanner do
  def scan(code)
    Parenthino::Scanner.scan code
  end

  it 'scans true and false literals' do
    scan(%q|#t|).should eq [[:boolean, true, [0, 0]]]
    scan(%q|#f|).should eq [[:boolean, false, [0, 0]]]
  end

  it 'scans empty program' do
    scan(%q||).should eq []
  end
end

#'(foldl + 0 (range 0 2 7))'
#=>
#[[:lparen, '('], [:identifier, '('], [:identifier, '+'], [:integer, 0],
# [:lparen, '('], [:identifier, 'range'], [:integer, 0], [:integer, 2],
# [:integer, 7], [:rparen, ')'], [:rparen, ')']]
